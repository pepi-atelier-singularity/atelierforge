<div align="center">

[<img src="https://www.inrae.fr/themes/custom/inrae_socle/logo.svg" height="140">](https://www.inrae.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://ist.inrae.fr/wp-content/uploads/sites/21/2020/02/DipSO_logo-gradient-rvb-scaled.jpg" height="80">](https://ist.inrae.fr/list-a-inrae/dipso/)

# Atelier Forge du Séminaire CATI/PEPI/Pépinière du mercredi 9 mars 2022

</div>

Un Atelier pour partager des compétences et des connaissances sur les outils et les services offert par les [forge](https://fr.wikipedia.org/wiki/Forge_(informatique))s 

## Séquences & horaires prévisionnels
de 9h à 12h30
|  Horaires      |  Orateurs    | Titre                    |  Supports et ressources  |
| -------------- | -------------- | ------------------------ | ------------------------ |
| 9:00 - 9:05 | Patrick | Lancement & Introduction | --- |
| 9:05 - 9:30 | Les participants | Tour de table & Questions brises glaces | --- |
| 9:30 - 9:50 | Armel | Un exemple d'usage de forges comme GitHub pour un projet open source de la recherche| --- |
| 9:50 - 10:10 | Benoît | Utilisation des fonctionnalités d'intégration et de publication continue de Gitlab pour une application web Javascript | --- |
| 10:10 - 10:20 | Isabelle | Utilisation de la forgemia dans le cadre de développements statistiques | --- |
| 10:20 - 10:30 | Jacques | Retour d’expériences de l'utilisation de la forge MIA et de Mattermost, inter-CATI et unité GAFL | --- |
| 10:30 - 10:50 | Jean-François| GitLab CI/CD et environnement R | --- |
| 10:50 - 11:00 | Raphaël | Quand Docker et Gitlab font la course, ils me prennent pour une DinD, et ça me rend chèvre | --- |
| 11:00 - 11:15 | --- | Pause, Définition des groupes de travail & choix des thèmes | --- |
| 11:15 - 11:45 | Les participants | Ateliers en // | --- |
| 11:45 - 12:15 | Michel | Chantier forge institutionnelle INRAE, avancement | --- |
| 12:15 - 12:30 | Les participants | Conclusion et Discussion finale | --- |

## Orateurs

- Patrick Chabrier, Toulouse, IUMAN
- Jacques Lagnel, PACA, PlantBreed
- Jean-françois Rey, Avignon, IMOTEP
- Isabelle Sanchez, Montpellier, CODEX
- Benoît Toutain, Toulouse, GEDEOP
- Armel Thoni, Montpellier, IUMAN
- Michel Bamouni, PACA, DipSO
- Raphaël Flores, Idf-VG, GREP

## Introduction

Pourquoi faire un atelier sur les forges dans le séminaire CATI/PEPI/Pépinières?
 - De l'importance du code et du caractère agnostique des forges en la matière
 - Le contexte OpenScience & OpenData, le FAIR, la qualité et les nouvelles exigences
 - Outils collectifs, outils collaboratif & rationalité
 - Dans le cadre des métiers du numériques difficile d'y échapper
 - DevOps : Administration Système + développement Logiciel
 - Gestionnaires de version & GIT
 - Automatisation & CI/CD
 - WEB

 Atelier in fine centré sur le partage d'expérience et les interractions entre les participants.

 In fine avoir une meilleur compréhension collective des différents services proposés par les forges.

 L'objetif n'est pas de comparer les instances et les solutions.

## Tour de table & question brise glace

- Présentation individuelle
- La place des forges dans votre activité à titre individuel & collectif, aujourd'hui et demain?
- Vos attentes concernant cet atelier, et les forges en particuliers

## Session retour d'expérience jusqu'à 11h

- Forme libre : avec ou sans slides ety visite de projets dans les forges
- Laisser de la place à la discussion

## A la pause

Constituer les groupes.

## Ateliers participatifs en // & en autogestion

Libre à chacun des groupes d'échanger sur des pratiques et des bonnes pratiques ou sur les enjeux et les attentes.
Libre à chacun des groupes d'utiliser la ou les forges en direct.

Proposition de sujet pour les sous-groupes :

1. Automatisation des processus
    - CI/CD
    - Runners
2. La gestion de projet
    - Issues
    - Mattermost (channels, Boards, Playbooks)
3. Les registres de distribution
    - Archives
    - Packaging
    - Hubs d'image Dockers ou Singularity
4. La communication
    - Pages webs
    - Sites statistiques
    - wiki
    - MD

## Chantier Forge institutionnelle

En fonction:
- La question du modèle économique
- La question des scénarios 

## Conclusion

<div align="center">

[<img src="https://www.inrae.fr/themes/custom/inrae_socle/logo.svg" height="140">](https://www.inrae.fr/)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
[<img src="https://ist.inrae.fr/wp-content/uploads/sites/21/2020/02/DipSO_logo-gradient-rvb-scaled.jpg" height="80">](https://ist.inrae.fr/list-a-inrae/dipso/)

</div>
